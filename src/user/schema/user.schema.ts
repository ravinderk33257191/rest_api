import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ timestamps: true })
export class User {
  @Prop({ type: String, required: true })
  name: string;
  @Prop({ type: String, required: true })
  mobile: string;
  @Prop({ type: String, required: true })
  address: string;
}
export const UserSchema = SchemaFactory.createForClass(User);
